FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg/
WORKDIR /app/code

# renovate: datasource=github-releases depName=nocodb/nocodb versioning=semver
ARG NOCODB_VERSION=0.262.1

RUN curl --fail -L https://github.com/nocodb/nocodb/releases/download/${NOCODB_VERSION}/Noco-linux-x64 -o nocodb && \
    chmod +x nocodb

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
