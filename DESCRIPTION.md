## About

NocoDB is an open source #NoCode platform that turns any database into a smart spreadsheet. 

## Features

* It is free & self-hostable. Let your imagination be thy limit
* Create grid view, gallery view, kanban view and calendar view on top your data
* Search, sort, filter columns and rows with ultra ease
* Invite your team with fine grained Access Control
* Share views publicly and also with password protection
* Build automations with you favourite apps : Microsoft Teams Slack, Discord, Whatsapp, Twilio, Email & SMS
* Provides REST & GraphQL APIs with Swagger & GraphiQL GUI

