[0.1.0]
* Initial version

[0.2.0]
* Enable invite only signup

[0.3.0]
* Update NocoDB to 0.84.14
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.84.14)
* [Type: Bug][Status: Resolved][Priority: Critical] Can't reset password #1337
* [Type: Bug] formula not working as long as capital letter(s) exist in column name #1332
* [i18n translation] Couldn't find the local language code (Farsi) in the provided google workspace list #1320
* [Status: Resolved] Error " request failed with status code 400 " raised when trying to import an Excel file #1313
* Initial tests
* Enable back telemetry following upstream default

[0.4.0]
* Update NocoDB to 0.84.15
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.84.15)
* SingleSelect in Column Type Can't Add option #1423
* [Feature] Better copy pasting from NocoDB #1419
* [Feature] Choose how many rows to show on each page. #1418
* Where are attachment file stored ? #1416
* Deleting of user not possible #1399
* No Access to any project using API Key #1344
* utf8mb4 character set instead of utf8 #1340

[1.0.0]
* Update NocoDB to 0.84.16
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.84.16)
* fix: Japanese Translation. by @blucky in #1458
* fix incorrect HTTP method in REST API docs by @dhrrgn in #1521
* fix: csv import logic by @wingkwong in #1556
* fix: multiple after-update webhooks being triggered in text by @mertmit in #1557
* fix: hotfix reverting breaking changes on webhook multiple-trigger by @mertmit in #1565
* fix: gallery logic by @wingkwong in #1512
* fix: select field default value removal by @mertmit in #1602

[1.1.0]
* Update NocoDB to 0.90.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.90.0)
* API Design (Auth / Public / Data / Meta)
* NocoDB SDK
* Support Nested Lookup
* Support Sort on Lookup, Formula, Nested Data
* Support Filter on Lookup, Rollup, Formula, Nested Data
* Support Formula with Lookup, Rollup, Column
* Support Duplicate Relation

[1.1.1]
* Update NocoDB to 0.90.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.90.1)
* [Scope: v0.90.0] Failed to exec filter request with a non-english column #1593
* [Type: Feature][Scope: v0.90.0] [Feature] search / sort / filter on the primary value of linked records #1522
* [Type: Feature][Scope: v0.90.0] [Feature] Automate creation of tables (via Rest API / JSON / Yaml...) #1430
* [i18n translation][Scope: v0.90.0] Adding Farsi to NocoDB interface #1321
* [Type: Enhancement][Scope: v0.90.0] Column re-order doesn't work on Postgres Views #1319
* [Type: Feature][Scope: v0.90.0] [Feature] Use rollup column in Formula #514
* [Type: Feature][Scope: v0.90.0] [Feature] Use Lookup column in Formula #512
* [Type: Feature][Scope: v0.90.0] [Feature] Order of many to many relations #511
* [Type: Feature][Scope: v0.90.0] Feature : Meta APIs #457

[1.1.2]
* Update NocoDB to 0.90.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.90.2)
* fix: remove duplicate and add missing delete api by @mertmit in #1731
* fix: Audit migration corrections by @pranavxc in #1732

[1.1.3]
* Update NocoDB to 0.90.3
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.90.3)
* [Type: Bug][Status: Fixed][Priority: Critical] Can't access projects since upgrade to 0.90.0 #1722
* [Status: Fixed] shared base- fields showAll/ HideAll error #1713

[1.1.4]
* Update NocoDB to 0.90.5
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.90.5)

[1.1.5]
* Update NocoDB to 0.90.7
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.90.7)

[1.1.6]
* Update NocoDB to 0.90.8
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.90.8)

[1.1.7]
* Update NocoDB to 0.90.10
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.90.10)

[1.1.8]
* Update NocoDB to 0.90.11
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.90.11)

[1.2.0]
* Update NocoDB to 0.91.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.91.0)
* Import Airtable to NocoDB
* Import CSVs, Excel to NocoDB
* New APIs : GroupBy and Exists
* Column improvements : Ratings
* Formula enhancements

[1.2.1]
* Update NocoDB to 0.91.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.91.1)
* Import from Airtable to NocoDB Enhancement
* Currency Enhancement
* Formula Enhancement
* Rating Enhancement
* UI Enhancement

[1.2.2]
* Update NocoDB to 0.9.7
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.91.7)
* [Status: Fixed] Webhook with condition has issues #2209
* [Type: Bug][Status: Fixed] error:0909006C:PEM routines:get_name:no start line - for google cloud platform in heroku - Single slash to double slash issue #2207
* [Status: Resolved][Type: In Development][Scope: UI / UX] UX: Scroll option for fields menu #2181
* [Type: Enhancement][Status: Resolved][Scope: UI / UX] [UX] Search field option in filter & sort #2180
* [Status: Fixed][Type: In Development][DB: All] formula field error when used in filter #2177
* [Status: Resolved][Scope: UI / UX] [form view] disable update to number on scroll #2108

[1.2.3]
* Update NocoDB to 0.9.9
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.91.9)
* [closed] Issue: Something in nocodb code has changed that is causing use of v-treeview in nocodb gui to break #2321
* [Type: Enhancement] Drag spreadsheet columns in Tables by mouse #2284
* [closed] Docker-compose mysql not working #2282
* [For: Community] More visible table borders to make it easier to read the table contents #2239
* [Type: Bug][Status: Resolved][Priority: Critical] TypeError after upgrade to 0.91.6 with Docker #2225
* [Scope : DevOps] npm issue in release #2223
* [Status: More Info Needed][Type: AT Import] Import Airtable base to NocoDB (postgresql database) issue #2199
* [Type: Feature] Turn essential apps credentials as env #2131
* [Type: Bug][Status: Reproducible] [Formula] Validation missing in one of scenarios; additional records created #2046
* [Type: Bug] Webhook error in postgres heroku for an user #1903

[1.2.4]
* Update NocoDB to 0.91.10
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.91.10)
* fix(sync): handle existing user on project by @mertmit in #2371
* fix: sync duplicate column_name after sanitize by @mertmit in #2369
* fix: treat decimal as double in mysql by @dstala in #2368
* fix: exclude date1904 fix from csv quick import by @wingkwong in #2357
* fix: delete project: in cache for disable shared base by @wingkwong in #2359
* fix: make email case-insensitive by @pranavxc in #2376

[1.3.0]
* Update NocoDB to 0.92.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.92.0)
* Binary Executables
* Homebrew
* JSON Import
* Duration Cell

[1.3.1]
* Update NocoDB to 0.92.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.92.1)

[1.3.2]
* Update NocoDB to 0.92.3
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.92.3)
* Add executable download script
* chore: keep one label

[1.3.3]
* Update NocoDB to 0.92.4
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.92.4)
* Filter Groups : our powerful grouped filtering is now available in GUI too (prior to this release it was only at API level)!
* Date Formatter : Customise your date formatting to your needs.
* Sort menu is now enhanced to toggle between asc/desc based on types.

[1.4.0]
* Update NocoDB to 0.96.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.96.2)
* New Frontend in Typescript
* Moved from Vue2 to Vue3 : Composition API 
* Nuxt3: Framework Migration
* Next generation utility-first CSS framework: Windi CSS
* Enhanced support for JDBC URL and SSL support.
* And we're hiring! :fire:

[1.4.1]
* Update NocoDB to 0.96.4
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.96.4)
* Impossible to create a new row since 0.96 #3565
* Use generate button pressing enter in dialog for creating a token #3562
* Can't upload CSV : POST on /dashboard/ fails -> 404 Not Found #3542
* Unable to view and download files in public share view link #3541
* Running nocodb without access to github #3540
* NocoDB release action : default to master #3537
* Horizontal scroll #2775
* Reorder multiselect options in column settings (manually or other) #1287
* Treeview item order #1021
* Add mobile support #850
* Support SSL Certs #215

[1.4.2]
* Update NocoDB to 0.97.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.97.0)

[1.5.0]
* Update NocoDB to 0.98.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.98.1)
* Kanban View: Its slick and its FREE! Display your data in different stacks and rearrange card items by drag-and-drop.
* Survey Form: Gorgeous & limitless survey forms on your own databases.
* Gallery Share View: Showcase your image gallery to the world with a simple click of a button!
* Copy From Multiple Cells: Now you can drag to select multiple cells and copy the data!

[1.5.1]
* Update NocoDB to 0.98.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.98.2)

[1.5.2]
* Update NocoDB to 0.98.3
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.98.3)
* Bug: Documentation online is not updated (at least release list)
* chore: downgrade and fix swagger-typescript-api version by @mertmit in #4218
* Fix: Grid cell - avoid overwriting LTAR and Lookup cell on cell update by @pranavxc in #4221

[1.5.3]
* Update NocoDB to 0.98.4
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.98.4)
* fix: allow env variables to recover missing super user by @mertmit in #2760
* chore(deps): bump terser from 5.14.1 to 5.15.1 in /packages/nc-gui by @dependabot in #4226
* refactor: remove copyright blocks by @wingkwong in #4229
* New Crowdin updates by @o1lab in #4227
* typo-galleryViewGet by @willnewii in #4233
* fix: ssl mode mapping by @mertmit in #4234
* feat(gallery): skip empty value to be displayed in the gallery view by @redhoyasa in #4242
* fix: app store plugin test by @wingkwong in #4219
* fix(nocodb): handle type in round function in postgres by @wingkwong in #4239
* New Crowdin updates by @o1lab in #4250
* fix(nc-gui): allow expand when is locked by @mateonunez in #4153
* refactor: nocodb project structure by @wingkwong in #4237
* New Crowdin updates by @o1lab in #4258
* Feat: Added and integrated playwright by @mustafapc19 in #3848
* chore(nc-gui): update nuxt to latest RC by @bcakmakoglu in #4279
* Executable build - generate sqlite binaries for multiple platforms by @pranavxc in #4280
* Set default database encoding/charset to utf8 by @pranavxc in #4284
* feat: upgrade knex by @mertmit in #3570
* Fix: Attachment cell - removing attachment error by @pranavxc in #4296
* fix(nc-gui): overlapping edge labels by @bcakmakoglu in #4289
* Fix: Grid view - tab switching error by @pranavxc in #4295
* Miscellaneous bug fixes by @pranavxc in #4302
* Treeview: reset search input on close and focus on load by @pranavxc in #4299
* Fix: Form view - required column reordering by @pranavxc in #4311

[1.6.0]
* Update NocoDB to 0.99.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.99.0)
* Edit all data types within a cell with ease of keyboard. No mouse required.
* Account Settings for Users and Super Admin

[1.6.1]
* Update NocoDB to 0.99.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.99.1)
* New Crowdin updates by @o1lab in #4469
* refactor(test): Playwright followup by @mustafapc19 in #4393
* Update MssqlClient.ts by @Naude555 in #4477
* New Crowdin updates by @o1lab in #4479
* Added docs for playwright by @mustafapc19 in #4476
* feat: useFieldQuery by @wingkwong in #4473
* fix(nc-gui): focus on the first input in expanded form nextTick by @wingkwong in #4485
* fix(nocodb): convert attachment type in api response by @wingkwong in #4488
* fix(nc-gui): set show-search always true by @wingkwong in #4490
* chore: cypress cleanup by @wingkwong in #4492
* fix(nocodb): return select option in given order by @wingkwong in #4491
* fix(nc-gui): page swtich bug + gallery display bug by @pranavxc in #4495

[1.6.2]
* Update NocoDB to 0.99.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.99.2)
* [bug Type: Bug][rocket Status: Ready for Next Release] bug Bug: Cannot read properties of null (reading 'parentNode') #4494
* [rocket Status: Ready for Next Release] bug Bug: onSearch should work with showSearch instead of use alone. #4489
* [bug Type: Bug][rocket Status: Ready for Next Release] bug Bug: In Record Detail/Edit overlay, cursor jumps into another input field after typing the first character #4483
* [bug Type: Bug][rocket Status: Ready for Next Release] bug Bug: Webhook payload includes attachment json as string #4480
* [rocket Status: Ready for Next Release] flashlight Feature: Search should be separate for each table #4449
* [bug Type: Bug][rocket Status: Ready for Next Release] bug Bug: SingleSelect options do not keep their order #4429

[1.7.0]
* Update NocoDB to 0.100.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.100.0)

[1.7.1]
* Update NocoDB to 0.100.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.100.1)

[1.7.2]
* Update NocoDB to 0.100.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.100.2)
* Release 0.100.1 by @github-actions in #4561
* fix(gui): avoid clearing cell data if it's a non-typable column by @pranavxc in #4556
* New Crowdin updates by @o1lab in #4562
* chore(deps): bump socket.io-parser from 4.0.4 to 4.0.5 in /packages/nocodb by @dependabot in #4350
* chore(deps): bump decode-uri-component from 0.2.0 to 0.2.2 by @dependabot in #4569
* chore(deps): bump decode-uri-component from 0.2.0 to 0.2.2 in /packages/nocodb by @dependabot in #4568
* chore(deps): bump @xmldom/xmldom from 0.7.6 to 0.7.9 in /packages/nc-cli by @dependabot in #4567
* chore(deps): bump decode-uri-component from 0.2.0 to 0.2.2 in /packages/nc-cli by @dependabot in #4566
* chore(deps): bump decode-uri-component from 0.2.0 to 0.2.2 in /packages/noco-docs by @dependabot in #4565
* chore(deps): bump decode-uri-component from 0.2.0 to 0.2.2 in /packages/nc-plugin by @dependabot in #4564
* Remove outline of search input in single select by @pranavxc in #4572
* feat(nocodb): include error message for resetting password by @wingkwong in #4571
* fix(nc-gui): unexpected focus on ListItem by @wingkwong in #4576

[1.8.0]
* Update base image to 4.0.0

[1.8.1]
* `NC_INVITE_ONLY_SIGNUP` is obsolete

[1.9.0]
* Update NocoDB to 0.101.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.101.0)

[1.9.1]
* Update NocoDB to 0.101.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.101.2)
* flashlight Feature: Trigger Webhook (after update) for changes in LinkToAnotherRecord field #4827
* flashlight Feature: Show all the rows on the same page #4813
* flashlight Feature: Move columns/rows using a drag & drop gesture #4811

[1.9.2]
* Update NocoDB to 0.104.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.104.2)

[1.9.3]
* Update NocoDB to 0.104.3
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.104.3)

[1.9.4]
* Update NocoDB to 0.105.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.105.0)

[1.9.5]
* Update NocoDB to 0.105.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.105.1)
* bug Bug: #5135
* Feature: column name length validator #5130
* Bug: SwaggerUI not working with latest release #5125
* Bug: Data display issue when sorting lines with empty fields #5104
* Bug: Previously shared views need to be re-shared with 0.98.4 #4446
* Feature: Row number in total not per page #4096
* Improve error handling when external db connection errors out #3369
* Bug: bug empty relations in view #2784
* An error does not exist in Spaces after NPM run dev is executed #2620
* Bug: The server closed the connection #2417
* Bug: Relation pop-up filter not working when using formula field #2316
* Use formula column as primary value #1809
* Fixed to left to display lines number. #1589
* Excel import : long text cells not displayed with column title containing commas and accents #1387
* Postgres relation query failing because of relation to non primary key #729

[1.9.6]
* Update NocoDB to 0.105.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.105.2)
* [closed] bug Bug: Accessing root url showing old dummy page #5142
* test: swagger by @dstala in #5139
* fix: remove dummy page by @pranavxc in #5143

[1.9.7]
* Update NocoDB to 0.105.3
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.105.3)
* feat: implement NC_DISABLE_AUDIT by @wingkwong in #5137
* feat(nocodb): set cache header for attachment urls by @pranavxc in #5151
* New Crowdin updates by @o1lab in #5123
* feat: upgrade timeout msg by @wingkwong in #5154
* fix: ts types for sql clients by @mertmit in #5134
* fix(nc-gui): add missing filterChildrenRead permission by @wingkwong in #5167
* feat(gui): Support multiple record mapping without closing LTAR modal by @pranavxc in #5153
* refactor: use extractProps by @wingkwong in #5168
* fix: missing display value by @mertmit in #5141

[1.9.8]
* Update NocoDB to 0.106.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.106.0)
* Feature Release : Undo/Redo, Webhooks revamped & UX Performance improved

[1.9.9]
* Update NocoDB to 0.106.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.106.1)

[1.10.0]
* Update NocoDB to 0.107.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.107.0)
* Copy Projects / Tables

[1.10.1]
* Update NocoDB to 0.107.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.107.1)
* fix: Load JWT secret from store on app init by @pranavxc in #5680
* fix: Read env file on app app load by @pranavxc in #5685

[1.10.2]
* Update NocoDB to 0.107.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.107.2)
* Bug: SwaggerUI not working with latest release #5125

[1.10.3]
* Update NocoDB to 0.107.3
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.107.3)
* LinkToAnotherRecord dosen't work in Shared View Forms #5694
* Formula functions prefer to be case-insensitive #5640

[1.10.4]
* Update NocoDB to 0.107.4
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.107.4)
* Super Admin No longer created during initialization #5722
* Storage upload unauthorized using xc-token authentication header(it works with xc-auth) #5713
* flashlight Feature: Select number of row to display in each page #5708
* Metadata not in sync and breaking most of the features #5701
* model list cache ignores `base_id` #5568

[1.10.5]
* Update NocoDB to 0.107.5
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.107.5)
* Form view not rendering available options #5740
* Uncaught ReferenceError: SwaggerUIBundle is not defined #5739

[1.11.0]
* Update NocoDB to 0.108.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.108.0)
* Any datetime related field is now in UTC format in its API input and output. This will be a breaking change.
* [closed] bug Bug: Crash with 0.108.0 when `NC_PUBLIC_URL` is set #5778
* [Status: Reproducible] bug Bug: Can't re-size columns in shared view #5772
* [closed] flashlight Feature: Remove initial boolean operator when adding first Filter (Group) in Filtering UI #5648
* [flashlight Type: Feature][telescope Scope : Project] Feature: Realtime collaboration #172
* [flashlight Type: Feature][telescope Scope : Project] Feature : SQL : Migrations from one env to another #146

[1.12.0]
* Update NocoDB to 0.109.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.109.0)
* Multiple cell copy paste
* Create link between tables by just dragging and dropping tables

[1.12.1]
* Update NocoDB to 0.109.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.109.1)
* [bug Type: Bug][building_construction Status: In Resolution] bug Bug: projects' Actions missing in dashboard's UI #5883
* [closed] bug Bug: Latest Build Failed #5882
* [Status: Reproducible][building_construction Status: In Resolution] bug Bug: Undo after a failed delete operation #5880
* [closed] bug Bug: Schema filter not working for SQL Views in database connections #5823
* Release 0.109.0 by @github-actions in #5878
* fix: on delete fail, return error instead of 200 OK by @dstala in #5881
* fix: Filter view list based on current schema in pg and mssql by @pranavxc in #5870
* fix: avoid adding undo if delete fails by @mertmit in #5884
* fix: Show project action icons based project role by @pranavxc in #5885
* fix: save any unsaved data on tab close, view switch, table switch or data reload by @pranavxc in #5867

[1.12.2]
* Update NocoDB to 0.109.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.109.2)
* [closed] bug Bug: Delete table error #5934
* [closed] bug Bug: Error when editing tables with foreign keys #5931
* [Status: Reproducible] bug Bug: Single select doesn't work #5926
* [building_construction Status: In Resolution] bug Bug: Not all records returned in row with relations #5906
* [closed] flashlight Feature: sql query as table #5900

[1.12.3]
* Update NocoDB to 0.109.3
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.109.3)
* [closed] bug Bug: Signup Option still there after deactivating #5976
* [closed] Error on Frontend init (oauth2 client ID) #5971
* [closed] Bug: 0.109.1 -> 0.109.2 : Migration from 0107004 to 0108002 failed #5957
* [closed] Bug: Date picker- auto close modal after date is selected #5949
* [closed] bug Bug: Impossible to input data to a single select column type in form #5939
* [closed] bug Bug: Delete record, undo; links are not re-inserted #5935

[1.12.4]
* Update NocoDB to 0.109.4
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.109.4)
* fix: prevent undo while editing by @mertmit in #5989
* feat: vertical fill using handle by @mertmit in #5896
* chore: test suite reorg by @dstala in #5997
* General UI/UX - enhancement/bugs by @pranavxc in #5952
* test: enable timezone spec for mysql & pg by @dstala in #6004
* Update README.md - Missing "OutFile" to save the Windows binaries. by @revyte in #5995
* Feat: Links column type by @pranavxc in #5848
* Revert "Feat: Links column type" by @pranavxc in #6011
* fix: use custom useCopy composable for copy by @pranavxc in #6015
* feat: Identify removed/added primary key in meta sync by @pranavxc in #6030
* fix: Avoid generating invalid token after signout by @pranavxc in #6032
* Fixed bug when trying to open a row with a binary encoded uuid as PK by @VictorMinemu in #6017

[1.12.5]
* Update NocoDB to 0.109.5
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.109.5)
* Bug: In table, my filtered data are changing when the other editor or creator creates a filter #6083
* Bug: Deleting tables/columns fails with "Cannot read properties of undefined (reading 'fk_index_name')" #6063
* Feature: can i combine nocodb with react frontend ? #6056
* How to use API to upload my photos to NocoDB? #6052
* Bug: hiding fields from main table removes attributes from webhook #6047
* Bug: field name 1st letter in Uppercase : no change in certain cases #6041
* Bug: swagger not working #5999
* Bug: NC_CONNECT_TO_EXTERNAL_DB_DISABLED without any function #5987
* Bug: Unable to enter text on form after interacting with single select field #5968
* Bug: Slow Performance since update 0.109.0 #5946
* Bug: Syncing NC_DB metadata removes visibility to all tables from external DB #5944

[1.12.6]
* Update NocoDB to 0.109.6
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.109.6)
* Bug: Webhook doesn't work when condition is turned on #6111
* Bug: Path Traversal Vulnerability in Attachment APIs #6101

[1.12.7]
* Update NocoDB to 0.109.7
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.109.7)

[1.13.0]
* Update NocoDB to 0.111.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.111.0)
* Group By in Grid View
* Introducing "Links" relational column
* Unified Layout for all DBs
* A New Details Tab for Table
* Bug: while primary key values contain "/" symbol, expand the row to get Record not found banner. #6202
* Bug: Shared view #6201
* Bug: Redo doesn't work on macOS #6200
* What could cause HTTP 431 response to socket.io POST ? #6193
* Bug: Cannot import xlsx ods csv tables in Docker (file in a VM-shared folder) #6192
* Bug: API Url mixes between project Id, project name in Swagger, doc ... #6187
* line height cannot be set, and content cannot be wrapped #6183
* Bug: migration issue #6181
* Bug: Hidden tables showing up #6179
* Bug: Disable starred & license options #6176
* Bug: Decimal precision handling with mysql #6172
* Bug: airtable import on sqlite breaks #6166
* Bug: CE / SQLite - filters on link #6165
* Bug: Default grid's activity.filter will affect RESTAPI's data #6142
* Bug: QR Code column type not working when input column is a formula #5963

[1.13.1]
* Update NocoDB to 0.111.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.111.1)
* Bug: Missing scrollbar in shared form and gallery view #6276
* Bug: Easter egg menu for fields is inaccessible #6269
* Bug: Cannot send multiple user invites to a project by separating with a comma #6267
* Bug: Locked view- double click on column header opens add/edit modal #6266
* Bug: Showing Empty could be removed #6264
* Bug: dateTime field, on reload displays GMT #6261
* Bug: Cannot Bulk Delete #6260
* Bug: form field is not changing on tab or press enter #6257
* Bug: Webhook call sending Parameters doesn't work #6246
* Bug: Search doesn't work in Gallery view #6242
* Bug: Login e-mail form field is not autocompleted #6241
* Bug: Upgrading from 0.109.7 to 0.111.0 fails #6233
* Bug: The Role is not super admin, just project's owner cannot change project name, still view old project name #6226
* Bug: Editors cannot use filter groups #6224
* Bug: Shared base UI #6204
* Bug: In Kanban, the time is 8 hours less #6186
* Bug: Sort from column header only works if other sorts are already applied #6119
* Bug: API name of Grid do not change after Duplicate #6038
* Bug: can't sign in on custom URL #5990
* Reopen all open tabs (of tables) #5916
* Bug: Grid view lock is broken #5912
* Bug: UI/UX Tidy - Grid layout : Column header #5908
* Bug: UI/UX Tidy - Grid layout : Cell formatting #5907
* Bug: [Nest] ERROR [i] Missing refresh token #5817
* UX for create project, table & view #5793
* Field Display Order in New Record Modals for Relation Fields #5603
* when creating linked tables, use NLP to name columns correctly #5409
* Bug: Same column sort re-applied using column context menu #5355
* Bug: Cannot Change A Users Role With Postgres Backend #5287
* Bug: the position of "next" and "previous" buttons in forms is confusing for RTL languages #5217
* UX: default value configuration #5013
* Bug: Knex timeout error when importing from Airtable #4650
* Bug: Memory crash when importing large base from Airtable #4649
* Allow to hide Jobs and Promotion boxes #4563
* Batch add selection #4540
* The expanded view should give more information than the cards themselves. #4092
* Feature: Workspaces #3915
* Need to show projects to user to which he was invited or has access only #3871
* add i18n menu for shared view #3461
* one to one relation #3425
* Re-open last open table when selecting project #3230
* Flex instead of tables #3212
* Bug: Hours auto-increment when focusing on datetime cell, without doing any change #3004
* Paste mutliple (100s) of rows at a time into an existing table #2575
* Bug: Export CSV: Not all links exported #2272
* Expanded fillable form for LongText cell #2271
* Improve latest API docs & swagger generated #2015
* Multiple Formula Fields Rendering Time #1879
* More Info Needed] Trying to connect nocodb(docker-compose) to patroni cluster in docker swarm. Error: pg_hba.conf rejects connection for host (ip address from overlay network). #1450
* Nocodb mysql connection timeout #775
* URL abstract type should support URI schemes #469

[1.13.2]
* Update NocoDB to 0.111.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.111.2)
* fix(nc-gui): fix search reset on view toggle by @reenphygeorge in #6292
* fix: code cleanup and UI bug fixes by @mertmit in #6307
* fix(nc-gui): expand icon made visible on click by @reenphygeorge in #6286
* Fixed view scroll not coming and auto scroll on active view by @mustafapc19 in #6317
* fix: warnings in ui by @mertmit in #6314
* fix: Add upgrader for creating missing LTAR index in metadb projects by @pranavxc in #6311
* fix: acl for cache apis by @mertmit in #6319
* Fix : Broken UI ACL by @pranavxc in #6318

[1.13.3]
* Update NocoDB to 0.111.3
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.111.3)
* fix: assign project_roles for apitoken by @mertmit in #6329
* fix: Add missing endpoints in swagger json by @pranavxc in #6332
* fix: super access by @mertmit in #6333

[1.13.4]
* Update NocoDB to 0.111.4
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.111.4)
* fix: Exclude any unnecessary props from app version JSON store value by @pranavxc in #6348
* fix: Project invite logic correction by @pranavxc in #6347
* fix: Missing Links column in swagger child api path param list by @pranavxc in #6349

[1.14.0]
* Update NocoDB to 0.202.4
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.202.4)
* Multi-field Editor, Unified Sidebar, Responsive views & Simplified APIs
* New Integrated Shortcuts
* New Simplified Data APIs

[1.14.1]
* Update NocoDB to 0.202.5
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.202.5)

[1.14.2]
* Update NocoDB to 0.202.7
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.202.7)

[1.14.3]
* Update NocoDB to 0.202.8
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.202.8)
* Bug: error when filtering by lookup field that is Link in the parent table. #7027
* Bug: API V2 Update API /rowid is missing #7021
* Bug: Api linked fields duplicated #7020
* Bug: Import excel throw 42701 Error #6998
* Bug: Uniform error in API v2 #6986
* Bug: import file content title not support unicode eg.(Chinese) #6959
* Runtime directive used on component with non-element root node. The directives will not function as intended. #6952
* Bug: multi fields editor, field type icons displayed are incorrect #6946

[1.14.4]
* Update NocoDB to 0.202.9
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.202.9)

[1.14.5]
* Update NocoDB to 0.202.10
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.202.10)
* chore(renovate): Update dependency @nuxt/image-edge to v1.1.0-28355789.b3279fe by @renovate in #7127
* docs: formula expansion as md sections by @dstala in #7116
* fix : added tooltip for column fields by @musharaf-nocodb in #7114
* fix: various leftover stuff by @mertmit in #7140
* fix: deprecate title length validator and use title on import editor by @mertmit in #7092
* fix: group by has many bt part by @mertmit in #7146
* fix: Added new join cloud btn by @dstala in #7072
* feat: display as progress by @mertmit in #7138
* fix: Extract api token roles by @pranavxc in #7169
* fix: filter mimetypes to preview in browser by @mertmit in #7162
* fix: use getSchema instead of validate for ajv by @mertmit in #7160
* fix: leftover issue with alternative clients by @mertmit in #7164
* fix: Missing values in LTAR columns by @pranavxc in #7161
* fix(nc-gui): non-image attachments in shared form by @wingkwong in #7177
* fix: while validating options check for option with wrapped in single quotes by @pranavxc in #7170
* fix : fixed column syncing in useViewColumnsOrThrow hook by @musharaf-nocodb in #7168
* fix : added showOnTruncateOnly prop in tooltip component by @musharaf-nocodb in #7163
* Fix: Misc minor design changes by @rameshmane7218 in #7148
* Nc fix/rich text followup by @dstala in #7175
* fix: Nested data apis(v2) permission issue by @pranavxc in #7179
* feat: support yyyy-mm in Datepicker by @wingkwong in #6870
* enhancement: rollup by @wingkwong in #6853
* fix: exact date filter not working on datetime by @DarkPhoenix2704 in #7189
* fix: Load userList only if allowed by @pranavxc in #7194
* fix: Ignore throwing error in upgrader if error is index already exist by @pranavxc in #7195
* fix: Escape single quote properly and when updating single select default value by @pranavxc in #7197
* feat: paste option in cell right click context menu by @rameshmane7218 in #7207
* fix: UI/UX fixes #7156 by @rameshmane7218 in #7173
* fix(nc-gui): add missing kanban clbk in expanded form by @wingkwong in #7200
* fix: rename ncProjectId to ncBaseId by @mertmit in #7213
* fix: Avoid setting default value(LTAR/Links) when adding new row in grouped by table by @pranavxc in #7208
* Fix/dropdown UI by @musharaf-nocodb in #7181

[1.15.0]
* Update NocoDB to 0.203.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.203.0)
* User fields
* RichText fields
* More formula functions supported
* Use of rollup within formula is now supported
* Use of formula results to build another formula is now enhanced
* Webhook update trigger (conditional)

[1.15.1]
* Update NocoDB to 0.203.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.203.2)
* Use different name for upload artifact to avoid conflict by @pranavxc in #7310
* Merge pull request #7310 from nocodb/develop by @pranavxc in #7311
* fix: shared form user field by @mertmit in #7316
* New Crowdin updates by @o1lab in #7319
* fix: strike and underline for rich text by @dstala in #7320
* fix: select default user playwright test fail issue by @rameshmane7218 in #7326
* Fix/expanded record ux by @rameshmane7218 in #7298
* fix: Formula validation correction by @pranavxc in #7329
* fix: Broken logo in Signin/Signup page by @pranavxc in #7333
* fix: Use withDefaults for setting default prop value by @pranavxc in #7335

[1.16.0]
* Update NocoDB to 0.204.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.204.0)
* Introducing "Created By" and "Last Modified By" Fields!
* Introducing "Created At" and "Last Modified At" Fields!

[1.16.1]
* Update NocoDB to 0.204.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.204.1)

[1.16.2]
* Update NocoDB to 0.204.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.204.2)
* Bug: Export CSV is missing the id or primary keys (cloud) #7749
* Bug: Unable to add new record to linked table by "add new link" #7748
* Bug: datetime copy paste bug #7739
* Bug: Sometimes unable to delete fields #7715
* Feature: Reset form in less than 5 seconds #7711
* Bug: Duplicating a form view only adds fields; all other form and field configurations are missing. #7686
* Bug: Form field order is different in backend than in the public view #7671
* Bug: modified row height, double click to switch to edit mode doesn't work #7658

[1.16.3]
* Update NocoDB to 0.204.3
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.204.3)

[1.16.4]
* Update NocoDB to 0.204.4
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.204.4)
* Bug: Export CSV is missing the id or primary keys (cloud) #7749
* Bug: Unable to add new record to linked table by "add new link" #7748
* Bug: Sometimes unable to delete fields #7715
* Feature: Reset form in less than 5 seconds #7711
* Bug: Duplicating a form view only adds fields; all other form and field configurations are missing. #7686
* Bug: Form field order is different in backend than in the public view #7671
* Bug: modified row height, double click to switch to edit mode doesn't work #7658

[1.16.5]
* Update NocoDB to 0.204.5
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.204.5)
* fix: keep hidden fields on undo by @dstala in https://github.com/nocodb/nocodb/pull/7779
* feat(nc-gui): Calendar Year View Updates by @DarkPhoenix2704 in https://github.com/nocodb/nocodb/pull/7734
* refactor: move query param check to a common function by @dstala in https://github.com/nocodb/nocodb/pull/7795
* fix: Remove invalid comma in swagger json by @pranavxc in https://github.com/nocodb/nocodb/pull/7798
* fix: Remove invalid character from swagger json by @pranavxc in https://github.com/nocodb/nocodb/pull/7799
* fix: handle if error is undefined/null in ErrorBoundary component by @pranavxc in https://github.com/nocodb/nocodb/pull/7802
* refactor: show body tab for customization by @dstala in https://github.com/nocodb/nocodb/pull/7790
* feat(nc-gui): new field icons by @dstala in https://github.com/nocodb/nocodb/pull/7803
* fix(nc-gui): update system icons by @DarkPhoenix2704 in https://github.com/nocodb/nocodb/pull/7804
* feat(nc-gui): refined ranges support & end Date for Date Field by @DarkPhoenix2704 in https://github.com/nocodb/nocodb/pull/7753
* Nc fix(docs): Update broken edit this page link by @rameshmane7218 in https://github.com/nocodb/nocodb/pull/7817
* Nc fix(nc-gui): long text field UI changes by @rameshmane7218 in https://github.com/nocodb/nocodb/pull/7824
* Nc feat: Prefill shared form by query params by @rameshmane7218 in https://github.com/nocodb/nocodb/pull/7786
* New Crowdin updates by @o1lab in https://github.com/nocodb/nocodb/pull/7827
* Nc fix(nc-gui): Incorrect route when navigating to view after visiting base members page by @rameshmane7218 in https://github.com/nocodb/nocodb/pull/7822
* New Crowdin updates by @o1lab in https://github.com/nocodb/nocodb/pull/7829
* Nc fix/auto collapse sidebar base table by @rameshmane7218 in https://github.com/nocodb/nocodb/pull/7820
* fix: add timeout to webhook calls by @mertmit in https://github.com/nocodb/nocodb/pull/7830
* fix: support Date filter operations in where query params for Created… by @dstala in https://github.com/nocodb/nocodb/pull/7834
* Nc feat(nc-gui): open base/table/view in a new tab on pressing Ctrl & clicking on them by @rameshmane7218 in https://github.com/nocodb/nocodb/pull/7818
* fix: duplicate base/model by @mertmit in https://github.com/nocodb/nocodb/pull/7823
* fix(nc-gui): lint by @DarkPhoenix2704 in https://github.com/nocodb/nocodb/pull/7836
* Nc fix(nc-gui): row height menu dropdown ui changes by @rameshmane7218 in https://github.com/nocodb/nocodb/pull/7839
* fix: missing calendar optmzin on oss by @DarkPhoenix2704 in https://github.com/nocodb/nocodb/pull/7838
* fix: allow shared form in iframe by @dstala in https://github.com/nocodb/nocodb/pull/7840
* Nc fix/7808 column reorder by @dstala in https://github.com/nocodb/nocodb/pull/7837
* Add support for URLENCODE function by @mitya57 in https://github.com/nocodb/nocodb/pull/6831
* Nc feat: Color option for Base logo by @rameshmane7218 in https://github.com/nocodb/nocodb/pull/7807
* feat: base level share by @DarkPhoenix2704 in https://github.com/nocodb/nocodb/pull/7833
* New Crowdin updates by @o1lab in https://github.com/nocodb/nocodb/pull/7846
* New Crowdin updates by @o1lab in https://github.com/nocodb/nocodb/pull/7852
* fix(nc-gui): Update offset of LinkedRecords in accordance with links and unlinks by @DarkPhoenix2704 in https://github.com/nocodb/nocodb/pull/7841
* Nc fix/browser console warnings by @dstala in https://github.com/nocodb/nocodb/pull/7855
* fix: disable default source menu/action options if it's disabled by @dstala in https://github.com/nocodb/nocodb/pull/7854
* fix: Sync both filename and connection.filename when updating SQLite datasource by @pranavxc in https://github.com/nocodb/nocodb/pull/7856
* Nc feat/survey form v2 by @rameshmane7218 in https://github.com/nocodb/nocodb/pull/7843
* fix(nc-gui): lint errors by @rameshmane7218 in https://github.com/nocodb/nocodb/pull/7857

[1.16.6]
* Update NocoDB to 0.204.7
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.204.7)
* Bug: User permissions on base get reset #7902
* [Question] I want to understand how you used sheet js to build you smart sheet? #7893
* The interface in the controller always redirects to the login page. #7887
* NocoDB random table identifier on V2 API Spec #7879
* Bug: multi field editor option colour change #7874
* Bug: Roles other than superadmin don't have ability to edit own comment #7870
* Feature: Swap keyboard shortcut for going to first and last pages #7853

[1.16.7]
* Update NocoDB to 0.204.8
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.204.8)
* Bug: When editing a field name, the formula using it are updated, but the calculated value is ERR #7923
* Bug: ctrl+click on base/table/view to open in new tab is not working in Window/Linux #7920
* Feature: While editing webhook show bulk hooks in disabled state #7913
* Bug: Attachment file names have redundant file extension if MIME type extension != existing file extension #7897
* Bug: nocodb-sdk uses PUT instead of PATCH for api.dbDataTableRow.update #7886
* Bug: form with default value configured for select field leads to error #7869

[1.16.8]
* Update NocoDB to 0.204.9
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.204.9)
* Bug: Insert data API(v2) not returning proper response #7945
* Bug: When editing a field name, the formula using it are updated, but the calculated value is ERR #7923
* Bug: ctrl+click on base/table/view to open in new tab is not working in Window/Linux #7920
* Feature: While editing webhook show bulk hooks in disabled state #7913
* Bug: Attachment file names have redundant file extension if MIME type extension != existing file extension #7897
* Failed to resolve entry for package "nocodb-sdk" Bug: #7896
* Bug: nocodb-sdk uses PUT instead of PATCH for api.dbDataTableRow.update #7886
* Bug: form with default value configured for select field leads to error #7869
* Where should I modify the sorting data query logic in order to customize the sorting functionality? #7737

[1.17.0]
* Update NocoDB to 0.205.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.205.0)
* Introducing Calendar View & Revamped Form View

[1.17.1]
* Update NocoDB to 0.205.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.205.1)

[1.18.0]
* Update NocoDB to 0.207.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.207.0)

[1.18.1]
* Update NocoDB to 0.207.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.207.1)

[1.18.2]
* Update NocoDB to 0.207.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.207.2)
* Bug: no access to bulk update #8504
* Bug: Send invitation causes "Cannot read properties of undefined (reading 'replace')" #8493
* Bug: font colour in select options #8034
* Bug: nested filtering doesn't work properly #5365

[1.18.3]
* Update NocoDB to 0.207.3
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.207.3)
* Feature: Tagging user(s) in comments
* Feature: Rich text for comments
* Feature: Comment removal

[1.19.0]
* Update NocoDB to 0.250.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.250.1)

[1.19.1]
* Update NocoDB to 0.250.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.250.2)

[1.20.0]
* Update NocoDB to 0.251.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.251.0)

[1.20.1]
* Update NocoDB to 0.251.1
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.251.1)

[1.20.2]
* Update NocoDB to 0.251.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.251.2)

[1.20.3]
* Update NocoDB to 0.251.3
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.251.3)

[1.21.0]
* Update NocoDB to 0.252.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.252.0)

[1.22.0]
* Update NocoDB to 0.255.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.255.0)
* Introducing New Field Type : Button
* Trigger Webhook/API: Automate workflows by configuring buttons to trigger custom webhooks. Send data from the current record to external applications or automation services like n8n/zapier (invoking their incoming webhooks)
* Dynamic URL Generation: Configure buttons to open custom URLs, which can be either static or dynamically generated based on the data in the current record.
* Launch Pre-Filled Forms: Easily set up buttons to open pre-filled NocoDB forms, streamlining data entry and reducing repetitive tasks.
* Customisable Appearance: Tailor the button’s text, colour, and icon to fit your specific needs, ensuring consistency and visual appeal within your table.
* Formula Integration: Leverage NocoDB's formula editor to create dynamic URLs and payloads, making your buttons even more powerful and responsive to changing data.

[1.22.1]
* Update NocoDB to 0.255.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.255.2)
* Tooltip of field description is not showing up in public views #9378
* when check the "Limit record selection to a view",configure filter... #9364
* Drag and drop item in calendar doesn't work #9360
* Formula error handling #9357
* display the column id #9335
* Select type field to increase the ability to associate to other tables #9327
* On windows file thumbnail not showing & no file found when trying to download attachments #9317
* TypeError: e.split is not a function when trying to run ,eq, query #9309

[1.23.0]
* Update NocoDB to 0.257.0
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.257.0)

[1.23.1]
* Update NocoDB to 0.257.2
* [Full changelog](https://github.com/nocodb/nocodb/releases/tag/0.257.2)

[1.23.2]
* Rename env to env.sh

[1.24.0]
* Update nocodb to 0.258.0
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.258.0)
* \[** Type: Bug**]  Bug: Link field error if table name is lengthy [#&#8203;9760](https://github.com/nocodb/nocodb/issues/9760)
* \[** Type: Bug**]  Bug: minio: Invalid endPoint if https is included such as in https://minio-api.some.domain [#&#8203;9757](https://github.com/nocodb/nocodb/issues/9757)
* \[**closed**] Bug: Cannot reset password. Always "Password reset successful!" [#&#8203;9751](https://github.com/nocodb/nocodb/issues/9751)
* \[** Type: Bug**]  Bug: No "equal to" option in AND conditional statements [#&#8203;9745](https://github.com/nocodb/nocodb/issues/9745)
* \[** Type: Bug**] BugAfter dragging the record to a different date, the date display on the right is not updated. [#&#8203;9732](https://github.com/nocodb/nocodb/issues/9732)
* \[** Type: Bug**]  Bug: Can't sync external database (Postgres 17) [#&#8203;9728](https://github.com/nocodb/nocodb/issues/9728)
* \[** Type: Bug**]  Bug: Prefill form is not working if field name contains ? at the end e.g., field name = `age ?` [#&#8203;9719](https://github.com/nocodb/nocodb/issues/9719)

[1.24.1]
* Update nocodb to 0.258.2
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.258.2)
* feat: gallery view infinite scroll by [@&#8203;DarkPhoenix2704](https://github.com/DarkPhoenix2704) in https://github.com/nocodb/nocodb/pull/9799
* New Crowdin updates by [@&#8203;o1lab](https://github.com/o1lab) in https://github.com/nocodb/nocodb/pull/9806
* Nc feat/personal view by [@&#8203;dstala](https://github.com/dstala) in https://github.com/nocodb/nocodb/pull/9807
* fix: Invalid Date error format in calendar view by [@&#8203;DarkPhoenix2704](https://github.com/DarkPhoenix2704) in https://github.com/nocodb/nocodb/pull/9809
* New Crowdin updates by [@&#8203;o1lab](https://github.com/o1lab) in https://github.com/nocodb/nocodb/pull/9813
* refactor: Token generation improvements and corrections  by [@&#8203;mertmit](https://github.com/mertmit) in https://github.com/nocodb/nocodb/pull/9784
* New Crowdin updates by [@&#8203;o1lab](https://github.com/o1lab) in https://github.com/nocodb/nocodb/pull/9816
* fix: Resolve bug in computed tooltip extraction logic by [@&#8203;pranavxc](https://github.com/pranavxc) in https://github.com/nocodb/nocodb/pull/9819
* Nc fix/miscellaneous bug fixes by [@&#8203;pranavxc](https://github.com/pranavxc) in https://github.com/nocodb/nocodb/pull/9827
* Nc fix/misc UI changes by [@&#8203;rameshmane7218](https://github.com/rameshmane7218) in https://github.com/nocodb/nocodb/pull/9829
* fix: Default base creation bug by [@&#8203;pranavxc](https://github.com/pranavxc) in https://github.com/nocodb/nocodb/pull/9835
* fix: update installation instructions by [@&#8203;DarkPhoenix2704](https://github.com/DarkPhoenix2704) in https://github.com/nocodb/nocodb/pull/9838
* 0.258.1 Pre-release by [@&#8203;github-actions](https://github.com/github-actions) in https://github.com/nocodb/nocodb/pull/9848
* Release 0.258.1 by [@&#8203;github-actions](https://github.com/github-actions) in https://github.com/nocodb/nocodb/pull/9849

[1.24.2]
* Update nocodb to 0.258.3
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.258.3)
* \[**Type: Bug**]  Bug: Multi select content vanishing on page turn [#&#8203;9902](https://github.com/nocodb/nocodb/issues/9902)
* \[**Type: Bug**]\[** Type: In Develop Branch**]  Bug: can't upload csv to table with checkbox field [#&#8203;9891](https://github.com/nocodb/nocodb/issues/9891)
* \[**Type: Bug**]\[** Status: In Resolution**]  Bug: Cannot use s3 ovh plugin, error: "cannot read property of undefined (reading builder)" [#&#8203;9890](https://github.com/nocodb/nocodb/issues/9890)
* \[**Type: Feature**]  Feature: Date Format [#&#8203;9869](https://github.com/nocodb/nocodb/issues/9869)

[1.24.3]
* Update nocodb to 0.258.10
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.258.10)
* \[**closed**]  Bug: Rounded corners not the same across the Quick Palette [#&#8203;10074](https://github.com/nocodb/nocodb/issues/10074)
* \[** Type: Bug**]  Bug: S3 storage doesn't work [#&#8203;10072](https://github.com/nocodb/nocodb/issues/10072)
* \[** Type: Bug**]  Bug: turn off the infinite scrolling function edit data bug [#&#8203;10070](https://github.com/nocodb/nocodb/issues/10070)
* \[** Type: Enhancement**] \[Enhancement] Input experience for link field [#&#8203;10012](https://github.com/nocodb/nocodb/issues/10012)
* \[** Type: Bug**]  Bug: Auto-upstall ends with message "docker: 'compose' is not a docker command." [#&#8203;10009](https://github.com/nocodb/nocodb/issues/10009)
* \[** Type: Bug**]  Bug: Auto-Upstall fails installing docker on AWS Linux with Error: unsupported distribution 'amzn' [#&#8203;10008](https://github.com/nocodb/nocodb/issues/10008)
* \[** Type: Bug**]\[**Status: Reproducible**]  Bug: Button Fields are removed when sync meta [#&#8203;9985](https://github.com/nocodb/nocodb/issues/9985)

[1.24.4]
* Update nocodb to 0.258.11
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.258.11)
* Limited Coverage: The existing audit logs captured less than 25% of the actions within the product. Key operationssuch as creating or modifying fields, filters, sorts, views, row comments, collaborators and webhooks etcwere not tracked at all. Over 150+ releases, the logs failed to keep up with NocoDBs expanding feature set.
* Incomplete Information: Even for the events that were logged, the data was partial and insufficient. For example record audit details often appeared fragmented, leaving gaps that fell short of enterprise-grade requirements.
* Comprehensive coverage of all critical actions within NocoDB.
* Detailed and reliable logs suited to enterprise use cases.
* \[**closed**]  Bug: chrismas toggle button won't show for users who don't have any acess  [#&#8203;10137](https://github.com/nocodb/nocodb/issues/10137)
* \[** Type: Bug**]  Bug: Error in Grid when pageInfo is undefined in list api [#&#8203;10132](https://github.com/nocodb/nocodb/issues/10132)
* \[**closed**]  Bug: Attachments - This request has been blocked; the content must be served over HTTPS. [#&#8203;10122](https://github.com/nocodb/nocodb/issues/10122)

[1.25.0]
* Update nocodb to 0.260.0
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.260.0)
* \[**closed**]  Bug: Update website footer year to 2025 [#&#8203;10177](https://github.com/nocodb/nocodb/issues/10177)
* \[**closed**]  Bug: Auto Upstall fails Docker installation on Amazon Linux [#&#8203;10160](https://github.com/nocodb/nocodb/issues/10160)
* \[** Type: Bug**]  Bug: Unable to Directly Add a Record to an External Database [#&#8203;9490](https://github.com/nocodb/nocodb/issues/9490)
* \[** Type: Feature**]  Feature: Multi field editor - show system fields [#&#8203;9264](https://github.com/nocodb/nocodb/issues/9264)
* \[**closed**]  Feature: Year formula for date fields [#&#8203;8721](https://github.com/nocodb/nocodb/issues/8721)
* \[**closed**] \[Enhancement] add the function of "number format options" under the rollup column [#&#8203;5312](https://github.com/nocodb/nocodb/issues/5312)
* \[** Type: Feature**]  Feature: Drag and Drop Rows [#&#8203;4748](https://github.com/nocodb/nocodb/issues/4748)
* \[** Type: Feature**]  Feature: Share View - Custom URL/Custom UUID [#&#8203;3963](https://github.com/nocodb/nocodb/issues/3963)

[1.25.1]
* Update nocodb to 0.260.1
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.260.1)
* \[**closed**]  Feature: Clarification on Adding New i18n Keys and Synchronization Across Language JSON Files [#&#8203;10204](https://github.com/nocodb/nocodb/issues/10204)
* \[**closed**]  Bug: Webhook not Triggering after Update [#&#8203;10201](https://github.com/nocodb/nocodb/issues/10201)
* \[**closed**]  Bug: AVG() not working as expected it does a ADD() on lookup fields. [#&#8203;10200](https://github.com/nocodb/nocodb/issues/10200)

[1.25.2]
* Update nocodb to 0.260.2
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.260.2)
* \[**closed**]  Bug: Error executing webhook: TypeError: Cannot read properties of undefined (reading 'workspace_id') [#&#8203;10268](https://github.com/nocodb/nocodb/issues/10268)
* \[**closed**]  Bug: unable to deploy nocodb frontend url on remote srver [#&#8203;10262](https://github.com/nocodb/nocodb/issues/10262)
* \[**closed**]  Feature:  [#&#8203;10261](https://github.com/nocodb/nocodb/issues/10261)
* \[**closed**]  Feature: NOCODB + OpenID Connect for education/non-commercial [#&#8203;10253](https://github.com/nocodb/nocodb/issues/10253)

[1.25.3]
* Update nocodb to 0.260.4
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.260.4)
* \[**closed**]  Bug: DATESTR function is not available for your database [#&#8203;10377](https://github.com/nocodb/nocodb/issues/10377)
* \[**closed**]  Bug: Data deletion bug when filtering and selecting all records [#&#8203;10375](https://github.com/nocodb/nocodb/issues/10375)
* \[**closed**]  Bug: Trying to edit any field in kanban view causes cursor to move into date field. [#&#8203;10374](https://github.com/nocodb/nocodb/issues/10374)
* \[**closed**]  Bug: NocoDB not starting after update to latest version [#&#8203;10355](https://github.com/nocodb/nocodb/issues/10355)
* \[**closed**]  Bug: lookup field are limited to 25 ! [#&#8203;10354](https://github.com/nocodb/nocodb/issues/10354)
* \[**closed**]  Bug: hash mismatch in Nix build [#&#8203;10339](https://github.com/nocodb/nocodb/issues/10339)

[1.25.4]
* Update nocodb to 0.260.5
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.260.5)
* \[**closed**]  Bug: DATESTR function is not available for your database [#&#8203;10377](https://github.com/nocodb/nocodb/issues/10377)
* \[**closed**]  Bug: Data deletion bug when filtering and selecting all records [#&#8203;10375](https://github.com/nocodb/nocodb/issues/10375)
* \[**closed**]  Bug: Trying to edit any field in kanban view causes cursor to move into date field. [#&#8203;10374](https://github.com/nocodb/nocodb/issues/10374)
* \[**closed**]  Bug: NocoDB not starting after update to latest version [#&#8203;10355](https://github.com/nocodb/nocodb/issues/10355)
* \[**closed**]  Bug: lookup field are limited to 25 ! [#&#8203;10354](https://github.com/nocodb/nocodb/issues/10354)
* \[**closed**]  Bug: hash mismatch in Nix build [#&#8203;10339](https://github.com/nocodb/nocodb/issues/10339)
* \[**closed**]  Feature: MySQL as ExtSource- Allow using text instead of set when using select fields [#&#8203;10338](https://github.com/nocodb/nocodb/issues/10338)

[1.25.5]
* Update nocodb to 0.260.6
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.260.6)
* \[**closed**]  Bug: Filter by date not working after update to 0.260.5 [#&#8203;10447](https://github.com/nocodb/nocodb/issues/10447)
* \[**closed**]  Feature: realtime updating [#&#8203;10440](https://github.com/nocodb/nocodb/issues/10440)
* \[**closed**]  Bug: cannot use search by fields with Russian names [#&#8203;10439](https://github.com/nocodb/nocodb/issues/10439)
* \[**closed**]  Bug: null doesn't work [#&#8203;10438](https://github.com/nocodb/nocodb/issues/10438)
* \[**closed**]  Bug: Trying to edit any field in kanban view causes cursor to move into date field [#&#8203;10424](https://github.com/nocodb/nocodb/issues/10424)
* \[** Type: Bug**]  Bug: log in page of app.nocodb.com is blank [#&#8203;9552](https://github.com/nocodb/nocodb/issues/9552)
* Release 0.260.5 by [@&#8203;github-actions](https://github.com/github-actions) in https://github.com/nocodb/nocodb/pull/10431
* fix internationalization by [@&#8203;pranavxc](https://github.com/pranavxc) in https://github.com/nocodb/nocodb/pull/10434
* fix: Add whitespace support in where query param parser by [@&#8203;pranavxc](https://github.com/pranavxc) in https://github.com/nocodb/nocodb/pull/10436
* fix (nocodb-sdk): Handle suboperator on logical level, more language support by [@&#8203;pranavxc](https://github.com/pranavxc) in https://github.com/nocodb/nocodb/pull/10441
* fix: attachment null by [@&#8203;mertmit](https://github.com/mertmit) in https://github.com/nocodb/nocodb/pull/10444
* fix: condition for attachment check by [@&#8203;mertmit](https://github.com/mertmit) in https://github.com/nocodb/nocodb/pull/10446
* Nc fix/date filter bug by [@&#8203;pranavxc](https://github.com/pranavxc) in https://github.com/nocodb/nocodb/pull/10445

[1.25.6]
* Update nocodb to 0.260.7
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.260.7)
* \[**closed**]  Bug: API updates are altering records not included in the payload. [#&#8203;10473](https://github.com/nocodb/nocodb/issues/10473)
* \[**closed**]  Bug: `limit` Parameter Ignored in REST API [#&#8203;10435](https://github.com/nocodb/nocodb/issues/10435)
* \[**closed**] Bug: Rollup a formula field [#&#8203;10429](https://github.com/nocodb/nocodb/issues/10429)
* \[** Type: Bug**]  Bug: Formula that evaluates to boolean [#&#8203;9815](https://github.com/nocodb/nocodb/issues/9815)
* \[**closed**]  Feature: Private minio cannot be used to download attachments [#&#8203;9590](https://github.com/nocodb/nocodb/issues/9590)
* fix: worker monaco by [@&#8203;mertmit](https://github.com/mertmit) in https://github.com/nocodb/nocodb/pull/10462
* fix: Bulk update - order record list same as input data by [@&#8203;pranavxc](https://github.com/pranavxc) in https://github.com/nocodb/nocodb/pull/10476
* fix: send table alias for rollup when column is formula by [@&#8203;fendy3002](https://github.com/fendy3002) in https://github.com/nocodb/nocodb/pull/10478
* fix (nc-gui): false readonly checkbox still show empty square by [@&#8203;fendy3002](https://github.com/fendy3002) in https://github.com/nocodb/nocodb/pull/10468
* refactor: add unsynced changes from ee by [@&#8203;fendy3002](https://github.com/fendy3002) in https://github.com/nocodb/nocodb/pull/10486
* fix: broken is requested logic by [@&#8203;mertmit](https://github.com/mertmit) in https://github.com/nocodb/nocodb/pull/10472

[1.26.0]
* Update nocodb to 0.262.0
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.262.0)
* :rocket: **Rebuilt from the ground up**  Say goodbye to sluggish interactions.
* **Canvas-powered rendering**  No bloat, just pure speed.
* :dash: **Ultra-smooth on old Windows machines**  Performance that just works.
* :strawberry: **Runs even on Raspberry Pi**  Handle data anywhere, with no slowdowns.
* :zap: **Were making NocoDB faster, leaner, and future-ready**so you can work without limits!
* \[**closed**]  Bug: Excel import error - Parameter 1 is not of type 'Blob' [#&#8203;10499](https://github.com/nocodb/nocodb/issues/10499)
* \[**closed**]  Bug: Since NocoDB Version 0.260 attachment in mp3 format not playable [#&#8203;10492](https://github.com/nocodb/nocodb/issues/10492)

[1.26.1]
* Update nocodb to 0.262.1
* [Full Changelog](https://github.com/nocodb/nocodb/releases/tag/0.262.1)
* \[**closed**]  Bug: Disorder in the record creation card within a related field. [#&#8203;10631](https://github.com/nocodb/nocodb/issues/10631)
* \[**closed**]  Bug: Description popup disabled. [#&#8203;10630](https://github.com/nocodb/nocodb/issues/10630)
* \[**closed**]  Bug: Upload CSV should skip system fields [#&#8203;10617](https://github.com/nocodb/nocodb/issues/10617)
* \[**closed**]  Bug: Can't copy past value to "many-to-many" field [#&#8203;10613](https://github.com/nocodb/nocodb/issues/10613)
* \[**Status: Reproducible**]  Bug: Form View with Attachment and Links Field, entry triggers "Failed to load list" [#&#8203;10608](https://github.com/nocodb/nocodb/issues/10608)
* \[**closed**]  Feature: Increase file upload size to 512MB to support big files [#&#8203;10595](https://github.com/nocodb/nocodb/issues/10595)
* \[**Status: Reproducible**]  Bug: String to currency transformation is broken [#&#8203;10593](https://github.com/nocodb/nocodb/issues/10593)
* \[**closed**]  Bug: The import date field doesn't respect the chosen format for all dates [#&#8203;10584](https://github.com/nocodb/nocodb/issues/10584)

