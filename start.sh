#!/bin/bash

set -eu

export NC_TOOL_DIR=/app/data
# format is documented here - https://docs.nocodb.com/getting-started/installation/#docker
export NC_DB="pg://$CLOUDRON_POSTGRESQL_HOST:$CLOUDRON_POSTGRESQL_PORT?u=$CLOUDRON_POSTGRESQL_USERNAME&p=$CLOUDRON_POSTGRESQL_PASSWORD&d=$CLOUDRON_POSTGRESQL_DATABASE"
export NC_PUBLIC_URL=$CLOUDRON_APP_ORIGIN
export NC_MIN=disableanimations # this disables the splash screen and screensaver

[[ -f /app/data/env ]] && mv /app/data/env /app/data/env.sh

if [[ ! -f /app/data/env.sh ]]; then
    echo -e "# Add custom environment variables here. See https://docs.nocodb.com/getting-started/environment-variables\n\n# export NC_DISABLE_TELE=true\n" > /app/data/env.sh
fi

source /app/data/env.sh

chown -R cloudron:cloudron /app/data

echo "==> Starting NocoDB"
cd /app/code
exec gosu cloudron:cloudron ./nocodb

